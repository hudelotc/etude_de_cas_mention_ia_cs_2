<!-- #region -->
<center><img src='./Figs/cs-logo.png' width=200></center>



<h6><center></center></h6>

<h1>
<hr style=" border:none; height:3px;">
<center>Speech Recognition</center>
<hr style=" border:none; height:3px;">
</h1>

This study case is based on a [Kaggle competition](https://www.kaggle.com/c/tensorflow-speech-recognition-challenge) and a [DL course](https://www.deeplearning.ai/).

In this study case, you will build a speech dataset and implement an algorithm for trigger word detection. Trigger word detection is the technology that allows devices like Amazon Alexa, Google Home, Apple Siri to wake up upon hearing a certain word or a certain set of words.

Given that audio clips are of temporal nature, you will use Recurrent Neural Networks, where the new state at time `t` can be dependent on all the previous steps `[0, t-1]` (given enough computationnal resources). For a brief introduction to these types of models, please refer to [your course on RNNs](https://centralesupelec.edunao.com/pluginfile.php/113399/mod_resource/content/4/04_rnn.pdf) or to [this short intro to RNNs](RNN_intro.pdf).

In this training lab, your trigger word will be "Activate". So, every time you detect the corresponding sound to the word "activate," you will output a "bell" sound.

This lab will be divided into three sections: 
- Synthesizing and processing the raw audio recordings to create train/validation datasets.
- Defining your recognition model.
- Training and testing your model.

## Packages

After the first training lab, all the packages you might need are already installed, the only package missing is `pydub`, that you will use to manipulate your audio recordings and create your train and validation sets. You can install it with:

`pip install pydub`
 
Now you can start the lab by heading to the notebook [speech_lab.ipynb](speech_lab.ipynb)

If you finished and you have some time, consider using the model to create a REST API: [Bonus: Keras & REST](bonus_keras_REST.md).

<!-- #endregion -->
